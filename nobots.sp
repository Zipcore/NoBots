#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <multicolors>

#define PL_VERSION "1.0"

public Plugin myinfo = 
{
	name        = "No Bots",
	author		= ".#Zipcore",
	description = "",
	version     = PL_VERSION,
	url         = "zipcore#googlemail.com"
};

Handle g_hBotQuota = null;

public void OnPluginStart()
{
	g_hBotQuota = FindConVar("bot_quota");
	HookConVarChange(g_hBotQuota, Action_OnSettingsChange);
}

public void Action_OnSettingsChange(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
	if (cvar == g_hBotQuota && StringToInt(newvalue) > 0)
	{
		SetConVarInt(g_hBotQuota, 0);
		ServerCommand("bot_kick");
	}
}